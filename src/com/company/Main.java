package com.company;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        int pin;
        System.out.print("Enter the pin 6 digits: ");
        Scanner s = new Scanner(System.in);

        pin = s.nextInt();
       String dataPin = Integer.toString(pin);

        if (isPinComplexityCase1(dataPin) && isPinComplexityCase2orCase4(dataPin) && isPinComplexityCase3(dataPin))  {
            System.out.println(dataPin+" : valid pin code, It's Okey" );
            System.exit(0);
        }else{
            System.out.println(dataPin+" : invalid pin code, It's easy to hack." );
            System.exit(0);
        }
    }

    public static boolean isPinComplexityCase2orCase4(String pin) {
        final String regex = "^(?:(\\d)\\1(?!\\1)){2}(\\d)\\2$";
        return !Pattern.compile(regex).matcher(pin).matches();
    }

    public static boolean isPinComplexityCase1(String pin) {
        final String regex = "^[0-9]{6}$";
        return Pattern.compile(regex).matcher(pin).matches();
    }

    public static boolean isPinComplexityCase3(String pin) {
        final String descending = "1234567890";
        final String ascending = "1234567890";
        if (descending.contains(pin.substring(0, 3)) ||
                descending.contains(pin.substring(1, 4)) ||
                descending.contains(pin.substring(2, 5)) ||
                descending.contains(pin.substring(3, 6)) ||
                ascending.contains(pin.substring(0, 3)) ||
                ascending.contains(pin.substring(1, 4)) ||
                ascending.contains(pin.substring(2, 5)) ||
                ascending.contains(pin.substring(3, 6))) {
            return false;

        }
        return true;

    }


}
